// ---------------------------------------------------
//
// Salnidad.h
//
// ---------------------------------------------------

#ifndef SALINIDAD_YA_INCLUIDO
#define SALINIDAD_YA_INCLUIDO
#include "C:/Users/Johnny/Desktop/GTI/CDIO/Proyecto-cdio/cdio_grupo05/Sprint3/Sensor/Sensor.h"
// ---------------------------------------------------
// ---------------------------------------------------
class Salinidad : public Sensor{
 private:
  int pinSignal;
  int powerPin;
  int sinSal;
  int Salado;

 public:

  /*Salinidad()
      : Sensor(0x48), pinSignal(1), powerPin(5), sinSal(7000), Salado(15000)
      {};*/
    
  Salinidad(Sensor & SensorPadre, int pinSignalI, int powerPinI, int sinSalI, int SaladoI) 
  : Sensor(SensorPadre), pinSignal(pinSignalI), powerPin(powerPinI), sinSal(sinSalI), Salado(SaladoI)
{ }
  
 // ---------------------------------------------------
// ---------------------------------------------------
int getPinSignal() {
  return (*this).pinSignal;
} // ()

// ---------------------------------------------------
// ---------------------------------------------------
int getPowerPin() {
  return (*this).powerPin;
} // ()

// ---------------------------------------------------
// ---------------------------------------------------
int getSinSal() {
  return (*this).sinSal;
} // ()

// ---------------------------------------------------
// ---------------------------------------------------
int getSalado() {
  return (*this).Salado;
} // ()

// ---------------------------------------------------
// ---------------------------------------------------
float medirSalinidad() {
    
  int pinSignal = (*this).pinSignal;
  int powerPin = (*this).powerPin;
  int sinSal = (*this).sinSal;
  int Salado = (*this).Salado;

  float sumatorioLecturas=0.0, salinidad; //Se declaran las variables necesarias para realizar el calculo de la salinidad
    
  //Generar un pulso entre dos cables, de esta forma si la conductancia es mayor, querra decir que hay mucha salinidad de lo contrario poca.
  digitalWrite( powerPin, HIGH ); // Se enciende el pin 
  delay(100); // Se pone en espera 
  sumatorioLecturas = ads1115.readADC_SingleEnded(pinSignal);//Se toma lectura
  digitalWrite( powerPin, LOW ); // Se apaga el pin
  delay(10);

  //Serial.println(sumatorioLecturas);//Esta impresion por pantalla se ha utilizado para realizar la calibración del programa
  salinidad = 100*sinSal/(sinSal-Salado)-sumatorioLecturas*100/(sinSal-Salado);//Se saca el % con los calores máximos y minimos

  //Este condicioal se hace para que no aparezcan valores por debajo de 0% y por encima de 100%
  if(salinidad<0){
    salinidad=0;
  }else if(salinidad>100){
    salinidad=100;  
  }
  
  return salinidad;//devuelve salinidad
} // ()

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
#endif

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
