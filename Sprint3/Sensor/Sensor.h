// ---------------------------------------------------
//
// Sensor.h
//
// ---------------------------------------------------

#ifndef Sensor_YA_INCLUIDO
#define Sensor_YA_INCLUIDO


#include <Adafruit_ADS1015.h>

// ---------------------------------------------------
// ---------------------------------------------------
class Sensor {
 protected:
  Adafruit_ADS1015 ads1115;

 public:

  Sensor();

// ---------------------------------------------------
// ---------------------------------------------------
Sensor( unsigned char);
    
Sensor(Adafruit_ADS1015);
    
Adafruit_ADS1015 getAds1115();
  
  
}; 
#endif

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
