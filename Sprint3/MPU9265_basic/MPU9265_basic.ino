//GND - GND
//VCC - VCC
//SDA - Pin 2
//SCL - Pin 14
 
#include <Wire.h>
 
#define    MPU9250_ADDRESS            0x68
#define    MAG_ADDRESS                0x0C
 
#define    GYRO_FULL_SCALE_250_DPS    0x00  
#define    GYRO_FULL_SCALE_500_DPS    0x08
#define    GYRO_FULL_SCALE_1000_DPS   0x10
#define    GYRO_FULL_SCALE_2000_DPS   0x18
 
#define    ACC_FULL_SCALE_2_G        0x00  
#define    ACC_FULL_SCALE_4_G        0x08
#define    ACC_FULL_SCALE_8_G        0x10
#define    ACC_FULL_SCALE_16_G       0x18


const int sleepTimeS = 7;  //tiempo que va estar durmiendo
const byte interruptPin = 4;  //pin que usamos para la interrupción

//Funcion auxiliar lectura
void I2Cread(uint8_t Address, uint8_t Register, uint8_t Nbytes, uint8_t* Data)
{
   Wire.beginTransmission(Address);
   Wire.write(Register);
   Wire.endTransmission();
 
   Wire.requestFrom(Address, Nbytes);
   uint8_t index = 0;
   while (Wire.available())
      Data[index++] = Wire.read();
}
 
 
// Funcion auxiliar de escritura
void I2CwriteByte(uint8_t Address, uint8_t Register, uint8_t Data)
{
   Wire.beginTransmission(Address);
   Wire.write(Register);
   Wire.write(Data);
   Wire.endTransmission();
}

void handleInterrupt(){
  uint8_t intStatus[1];  //vuelve a subir a nivel alto despues de la interupción
  I2Cread(MPU9250_ADDRESS, 0x3A, 1, intStatus);
  Serial.println("interrupción");

}
 
 
void setup()
{
  
   Wire.begin();
   Serial.begin(9600);
  Serial.println("Inicializando...");
  Serial.println("Configurando acelerónetro...");

  pinMode(interruptPin, INPUT_PULLUP); //siempre estara en un nivel alto
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, CHANGE); //la interrupcion se hara por el pin 4 y llamara a la funcion(handle)cuando(CHANGE)
  
   // Configurar acelerometro
   I2CwriteByte(MPU9250_ADDRESS, 28, ACC_FULL_SCALE_16_G);
   
   // Configurar giroscopio
     Serial.println("Configurando giroscopio...");
   I2CwriteByte(MPU9250_ADDRESS, 27, GYRO_FULL_SCALE_2000_DPS);
   // Configurar magnetometro
   
    Serial.println("Configurando magnetómetro...");
   //I2CwriteByte(MPU9250_ADDRESS, 0x37, 0x02);
   //I2CwriteByte(MAG_ADDRESS, 0x0A, 0x01);

   //registros sacados de los pdfs 
  I2CwriteByte(MPU9250_ADDRESS, 0x6B, 0x00);
  I2CwriteByte(MPU9250_ADDRESS, 0x6C, 0x07);
  I2CwriteByte(MPU9250_ADDRESS, 0x1D, 0x09);
  I2CwriteByte(MPU9250_ADDRESS, 0x38, 0x40);
  I2CwriteByte(MPU9250_ADDRESS, 0x69, 0xC0);
  I2CwriteByte(MPU9250_ADDRESS, 0x1F, 0x09);
  I2CwriteByte(MPU9250_ADDRESS, 0x1E, 0x08);
  I2CwriteByte(MPU9250_ADDRESS, 0x6B, 0x20);
  I2CwriteByte(MPU9250_ADDRESS, 0x37, 128);
}
 
 
void loop()
{
   // ---  Lectura acelerometro y giroscopio --- 
   uint8_t Buf[14];
   int FS_ACC = 16;
   int FS_GYRO = 2000;



   I2Cread(MPU9250_ADDRESS, 0x3B, 14, Buf);
 
//   // Convertir registros acelerometro
   float ax = (Buf[0] << 8 | Buf[1]);
   float ay = (Buf[2] << 8 | Buf[3]);
   float az = Buf[4] << 8 | Buf[5];

   ax = ax*FS_ACC/32768 ;
   ay = ay*FS_ACC/32768 ;
   az = az*FS_ACC/32768 ;
   //double mediax=(31.98+31.98+31.98+31.98+31.99)/5;
   //double axcalibrado=ax-mediax;
   double mediaz=(1.26+1.24+1.24+1.25+1.25+1.25)/6;  //calibración del offset (media)
   double azcalibrado=az-mediaz;
 
//   // Convertir registros giroscopio
   float gx = (Buf[8] << 8 | Buf[9]);
   float gy = (Buf[10] << 8 | Buf[11]);
   float gz = Buf[12] << 8 | Buf[13];
 
   gx = gx*FS_GYRO/32768;
   gy = gy*FS_GYRO/32768;
   gz = gz*FS_GYRO/32768;
   double mediagy=(1.04+1.28+1.16+1.34+0.92)/5;   //calibración del offset
   double gycalibrado=gy-mediagy;
   double mediagz=(3999.39+3999.45+3999.45+3999.27)/4; //calibración del offset
   double gzcalibrado=gz-mediagz;
// 

   ESP.deepSleep(sleepTimeS * 1000000);  //
   delay(5000); // tiempo en actualizar los datos
    
}
