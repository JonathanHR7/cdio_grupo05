// ---------------------------------------------------
//
// humedad.cpp
//
//
// ---------------------------------------------------
#include "humedad.h"

// ---------------------------------------------------
//Constructor por defecto
humedad::humedad(){
    Adafruit_ADS1015 ads(0x48);
    (*this).ads1115=ads;
    (*this).pinSignal=0;
    (*this).valorSeco = 20430;
    (*this).valorHumedo= 9804;
}

//Constructor parametrizado
humedad::humedad(Adafruit_ADS1015 ads, int pinSignalI, int valorSecoI, int valorHumedoI){
    (*this).ads1115=ads;
    (*this).pinSignal=pinSignalI;
    (*this).valorSeco = valorSecoI;
    (*this).valorHumedo= valorHumedoI;
}


// ---------------------------------------------------
// ---------------------------------------------------
int humedad::getPinSignal() {
  return (*this).pinSignal;
} // ()

// ---------------------------------------------------
// ---------------------------------------------------
int humedad::getValorSeco() {
  return (*this).valorSeco;
} // ()

// ---------------------------------------------------
// ---------------------------------------------------
int humedad::getValorHumedo() {
  return (*this).valorHumedo;
} // ()


// ---------------------------------------------------
// ---------------------------------------------------
int humedad::medirHumedad(){

  Adafruit_ADS1015 ads1115 = getAds1115();
  int pinSignal = getPinSignal();
  
  //Se obtienen de los atributos de clase los valores máximos y mínimos de humedad 
  int valorSeco = getValorSeco();  // valor en aire
  int valorHumedo = getValorHumedo();// valor en agua

  
  int16_t adc0 = ads1115.readADC_SingleEnded(pinSignal);//Se define el puerto del ads1115 al que se va a conectar el sensor de humedad.

  int humedad = 100*valorSeco/(valorSeco-valorHumedo)-adc0*100/(valorSeco-valorHumedo);//Se calcula la humedad con su formula.

  //Este condicioal se hace para que no aparezcan valores por debajo de 0% y por encima de 100%
  if(humedad<0){
    humedad = humedad+10;
   }else if(humedad>100){
     humedad = 100;
   }
  
   return humedad; //devuelve humedad
}
