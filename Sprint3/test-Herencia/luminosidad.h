//
// luminosidad.h
//

 

#ifndef luminosidad_YA_INCLUIDO //para que no incluya otra vez la misma libreria 
#define luminosidad_YA_INCLUIDO
#include "Sensor.h"

 

class luminosidad : public Sensor{
  private:
    int VOLTAJEMAXIMO;
    int  BITS;
    double B;
    double  M;
    double maxintensidad;
    int pinSignalVoltaje; //el pin 3 que usamos en la placa.

 

  public:
  luminosidad();
  luminosidad(Adafruit_ADS1015,int,int,double,double,double,int);
  double luminosidadFinal();
  
  };
  #endif
