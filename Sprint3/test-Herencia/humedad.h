//
// humedad.h
//

#ifndef humedad_YA_INCLUIDO
#define humedad_YA_INCLUIDO
#include "Sensor.h"

class humedad : public Sensor{
    
private:
  int pinSignal;
  int valorSeco;
  int valorHumedo;


public:
  humedad();
  
  humedad(Adafruit_ADS1015, int, int, int);

  int getPinSignal();

  int getValorSeco();

  int getValorHumedo();
  
  int medirHumedad();

};
#endif
