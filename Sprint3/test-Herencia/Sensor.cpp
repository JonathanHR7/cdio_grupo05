// ---------------------------------------------------
//
// Sensor.cpp
//
//
// ---------------------------------------------------
#include "Sensor.h"
#include <Wire.h>    


// ---------------------------------------------------
// ---------------------------------------------------
Sensor::Sensor()
{ 
    Adafruit_ADS1015 ads(0x48);
    (*this).ads1115=ads;
    (*this).PrimeraInteraccion=false; 
}

// ---------------------------------------------------
// ---------------------------------------------------
Sensor::Sensor( unsigned char addr ) {
    Adafruit_ADS1015 ads(addr);
    (*this).ads1115=ads;
    (*this).PrimeraInteraccion=false; 
  }

Sensor::Sensor( Adafruit_ADS1015 ads ) {
    (*this).ads1115 = ads;
    (*this).PrimeraInteraccion=false; 
  }
  
Adafruit_ADS1015 Sensor::getAds1115(){
    return (*this).ads1115;
};



// ---------------------------------------------------
// ---------------------------------------------------
