//
//  luminosidad.cpp
//


#include "luminosidad.h"

//-------------------------------------------------------

 

luminosidad::luminosidad(){
    Adafruit_ADS1015 ads(0x48);
   (*this).ads1115=ads;
   (*this).VOLTAJEMAXIMO=4096;
   (*this).BITS= 32767;
   (*this).B=0.79;
   (*this).M=0.033;
   (*this).pinSignalVoltaje=3;
   (*this).maxintensidad=3.64;
  }

 

luminosidad::luminosidad(Adafruit_ADS1015 ads1, int VOLTAJEMAXIMO1, int BITS1, double B77, double M1, double maxintensidad1, int pinSignalVoltaje1){
   (*this).ads1115 = ads1;
   (*this).VOLTAJEMAXIMO = VOLTAJEMAXIMO1;
   (*this).BITS = BITS1;
   (*this).B = B77;
   (*this).M = M1;
   (*this).maxintensidad = maxintensidad1;
   (*this).pinSignalVoltaje = pinSignalVoltaje1;
} 


double luminosidad::luminosidadFinal(){
    int  voltajeMaximo=(*this).VOLTAJEMAXIMO;
    int  bits=(*this).BITS;
    double  B=(*this).B;
    double  M=(*this).M;
    int pinSignal=(*this).pinSignalVoltaje;
    double maxintensidad=(*this).maxintensidad;

 

    //CALCULAR VOLTAJE
    int16_t valorLeido = ads1115.readADC_SingleEnded(pinSignal);//se define el puerto
    double voltajeFinal;
    voltajeFinal=(valorLeido*voltajeMaximo)/bits;//fórmula que transforma el número recibido en voltaje.
     if (voltajeFinal<0){
      voltajeFinal=0;
      }
    voltajeFinal=voltajeFinal/1000;
//-------------------------------------------------------------------------------
//intensidad
    double intensidadFinal= 100*voltajeFinal/maxintensidad;//formula para saber la cantidad de luz en porcentaje
    return intensidadFinal;//devuelve voltaje
    }
