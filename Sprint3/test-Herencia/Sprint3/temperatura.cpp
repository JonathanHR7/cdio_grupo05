//
//  temperatura.cpp
//

#include "temperatura.h" 

// ---------------------------------------------------
temperatura::temperatura(){  // constructor
 Adafruit_ADS1015 ads(0x48);
 (*this).ads1115=ads;
 (*this).pinSignalVoltaje=2;
 (*this).VOLTAJEMAXIMO=4096;
 (*this).BITS= 32767;
 (*this).B=0.79;
 (*this).M=0.033;
 }
temperatura::temperatura(Adafruit_ADS1015 ads,int VOLTAJEMAXIMO2 ,int BITS2,double B2,double M2,int pinSignalVoltaje2){
 (*this).ads1115=ads;
 (*this).pinSignalVoltaje=pinSignalVoltaje2;
 (*this).VOLTAJEMAXIMO=VOLTAJEMAXIMO2;
 (*this).BITS=BITS2 ;
 (*this).B=B2;
 (*this).M=M2;

  }


double temperatura::temperaturaFinal(){
  int  voltajeMaximo=(*this).VOLTAJEMAXIMO;
  int  bits=(*this).BITS;
  double  b=(*this).B;
  double  m=(*this).M;
  int pinSignal=(*this).pinSignalVoltaje;
  //-------------------------------------------------------------------------------
  //CALCULAR VOLTAJE
  int16_t valorLeido = ads1115.readADC_SingleEnded(pinSignal);//se define el puerto
  double voltajeFinal;
  voltajeFinal=(valorLeido*voltajeMaximo)/bits;//fórmula que transforma el número recibido en voltaje.
  voltajeFinal=voltajeFinal/1000;
  //-------------------------------------------------------------------------------
  //temperatura
   double temperatura=(voltajeFinal-b)/m;  //formula que calcula la temperatura 
   //Calibración del programa
   if (temperatura<40){ //Para ajustar la temperatura y para calibrar el rango de error cuando es <40 se le resta 0.3
    temperatura=temperatura-0.3;
   }else{
    temperatura=temperatura+4.8; //si es >40 se le suma 4.8
   }
   return temperatura; //devuelve la temperatura 
   //-------------------------------------------------------------------------------

}
