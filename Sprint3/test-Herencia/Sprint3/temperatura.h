//
// temperatura.h
//

#ifndef temperatura_YA_INCLUIDO //para que no incluya otra vez la misma libreria 
#define temperatura_YA_INCLUIDO
#include "Sensor.h"


class temperatura : public Sensor{
private:
  int  VOLTAJEMAXIMO;
  int BITS;
  double B;
  double M;
  int pinSignalVoltaje;  //el pin 2 que usamos en la placa.
public:
  temperatura(); //no le pasamos nada y elnos construye el objeto (ejemplo Punto origen;)
  temperatura(Adafruit_ADS1015,int,int,double,double,int); //cuando queremos crear un onjeto con ciertas condiciones diferentes del defecto
  double temperaturaFinal();

};
#endif
