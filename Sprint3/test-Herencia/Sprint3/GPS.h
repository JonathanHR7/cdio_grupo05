#include <TinyGPS++.h>  //Librería del GPS 
#include <SoftwareSerial.h> 
#define RX_PIN  12 // GPS RXI 
#define TX_PIN  13 // GPS TX0 
#define INIT_PIN 15 // Pin para  Inicializar el GPS 
#define GPS_BAUD  4800  //  velocidad de comunicación serie  

  

TinyGPSPlus gps; // Definimos el objeto gps 

  

SoftwareSerial ss(RX_PIN,TX_PIN); // Creamos una UART software para comunicación con el GPS 

  

///////////////////////////////////////////////////////////////////////////////////////// 

///// FUNCIONES 

  

// Función espera 1s para leer del GPS 

static void smartDelay(unsigned long ms) 

{ 

  unsigned long start = millis(); 

  do 

  { 

    while(ss.available()) 

    { 

      gps.encode(ss.read());  // leemos del gps 

    } 

  } while(millis() - start < ms); 

} 

// Función para encender/apagar mediante un pulso 

void switch_on_off() 

{ 

   // Power on pulse 

  digitalWrite(INIT_PIN,LOW); 

  delay(200); 

  digitalWrite(INIT_PIN,HIGH); 

  delay(200);  

  digitalWrite(INIT_PIN,LOW); 

} 


  

  
