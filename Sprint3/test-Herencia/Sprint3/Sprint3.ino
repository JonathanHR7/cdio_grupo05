#include "Sensor.h"
#include "Salinidad.h"
#include "humedad.h"
#include "temperatura.h"
#include "luminosidad.h"
#include "Acelerometro.h"
#include "GPS.h"

Adafruit_ADS1115 SensorPadre(0x48); 

void setup() {
  
  Wire.begin();
  Serial.begin(9600);
  SensorPadre.begin();
  SensorPadre.setGain(GAIN_ONE);
//----------------------Acelerometro config----------------------//
//--------------------------------------------------------------//
    Serial.println("//----------------------Acelerometro----------------------//");
    Serial.println("Inicializando...");
    Serial.println("Configurando acelerónetro...");
 
  

  pinMode(interruptPin, INPUT_PULLUP); //siempre estara en un nivel alto
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, CHANGE); //la interrupcion se hara por el pin 4 y llamara a la funcion(handle)cuando(CHANGE)
  
   // Configurar acelerometro
   I2CwriteByte(MPU9250_ADDRESS, 28, ACC_FULL_SCALE_16_G);
   
   // Configurar giroscopio
   Serial.println("Detectado movimiento");
   I2CwriteByte(MPU9250_ADDRESS, 27, GYRO_FULL_SCALE_2000_DPS);
   // Configurar magnetometro
   
    Serial.println(" Interrupción");
    Serial.println("//----------------------GPS----------------------//");
   //I2CwriteByte(MPU9250_ADDRESS, 0x37, 0x02);
   //I2CwriteByte(MAG_ADDRESS, 0x0A, 0x01);

   //registros sacados de los pdfs 
  I2CwriteByte(MPU9250_ADDRESS, 0x6B, 0x00);
  I2CwriteByte(MPU9250_ADDRESS, 0x6C, 0x07);
  I2CwriteByte(MPU9250_ADDRESS, 0x1D, 0x09);
  I2CwriteByte(MPU9250_ADDRESS, 0x38, 0x40);
  I2CwriteByte(MPU9250_ADDRESS, 0x69, 0xC0);
  I2CwriteByte(MPU9250_ADDRESS, 0x1F, 0x09);
  I2CwriteByte(MPU9250_ADDRESS, 0x1E, 0x08);
  I2CwriteByte(MPU9250_ADDRESS, 0x6B, 0x20);
  I2CwriteByte(MPU9250_ADDRESS, 0x37, 128);
//--------------------------------------------------------------//
//--------------------------------------------------------------//
//------------------------------GPS config----------------------//
//--------------------------------------------------------------//
  ss.begin(GPS_BAUD); // Inicializar la comunicación con el GPS 
  
  pinMode(INIT_PIN,OUTPUT);  

  switch_on_off(); // Pulso para encender el GPS 

  Serial.println("Fecha      Hora       Latitud   Longitud   Alt    Rumbo   Velocidad"); 

  Serial.println("(MM/DD/YY) (HH/MM/SS)     (deg)       (deg)  (ft)                   (mph)"); 

  Serial.println("-------------------------------------------------------------------------");  

}

void loop() {
  
 //Declaración de los Sensores//
  humedad sensorHumedad(SensorPadre,0,20430,9804);
  Salinidad sensorSalinidad(SensorPadre,1,5,11,7000);
  temperatura sensorTemperatura(SensorPadre,4096,32767,0.79,0.033,2);
  luminosidad sensorLuminosidad(SensorPadre,4096,32767,0.79,0.033,3.64,3);
//-------------------------- Lectura de eventos --------------------------//
// Cada sensor lla a su metodo correspondiente para tomar lectura de los eventos//
  int valorHumedad = sensorHumedad.medirHumedad();
  int valorSalinidad = sensorSalinidad.medirSalinidad();
  double temperaturaLeida = sensorTemperatura.temperaturaFinal();
  double valorLuminosidad = sensorLuminosidad.luminosidadFinal();

  //---------------------------------------------------------------------  
  //GPS
  //---------------------------------------------------------------------
    char gpsDate[10];  
    char gpsTime[10]; 

  

  if(gps.location.isValid()){ // Si el GPS está recibiendo los mensajes NMEA 

      sprintf(gpsDate,"%d/%d/%d", gps.date.month(),gps.date.day(),gps.date.year()); // Construimos string de datos fecha 
  
      sprintf(gpsTime,"%d/%d/0%d", gps.time.hour(),gps.time.minute(),gps.time.second());  // Construimos string de datos hora 
  
  
      Serial.print(gpsDate); 
  
      Serial.print('\t'); 
  
      Serial.print(gpsTime); 
  
      Serial.print('\t'); 
  
      Serial.print(gps.location.lat(),6); 
  
      Serial.print('\t'); 
  
      Serial.print(gps.location.lng(),6); 
  
      Serial.print('\t'); 
  
      Serial.print(gps.altitude.feet()); 
  
      Serial.print('\t'); 
  
      Serial.print(gps.course.deg(),2); 
  
      Serial.print('\t'); 
  
      Serial.println(gps.speed.mph(),2); 

  } 

  else  // Si no recibe los mensajes 
{ 

    Serial.print("Satellites in view: "); 

    Serial.println(gps.satellites.value()); 

  } 

  smartDelay(1000); 
  
  //---------------------------------------------------------------------
  //Humedad
  //---------------------------------------------------------------------
  Serial.println("//----------------------Humedad----------------------//");
  Serial.print("La Humedad leidad es de (%): "); //Lo muestra en pantalla
  Serial.print(valorHumedad); //muestra en pantalla el valor de la Humedad
  Serial.println("%");       //muestra ("") en pantalla
  //---------------------------------------------------------------------  
  //Salinidad
  //---------------------------------------------------------------------
  Serial.println("//----------------------Salinidad----------------------//");
  Serial.print("La salinidad leida es de (%): "); //muestra en pantalla
  Serial.print(valorSalinidad); //Muestra el resultado de los calculos en pantalla
  Serial.println("%"); //muestra en pantalla
  //---------------------------------------------------------------------  
  //Temperatura
  //---------------------------------------------------------------------
  Serial.println("//----------------------Temperatura----------------------//");
  Serial.print("La Temperatura es "); //muestra en pantalla
  Serial.print(temperaturaLeida-29); //muestra en pantalla la temperatura
  Serial.println("º");
  //---------------------------------------------------------------------  
  //Luminosidad
  //---------------------------------------------------------------------
  Serial.println("//----------------------Luminosidad----------------------//");
  Serial.print("La luminosidad leida es de (%): "); //muestra en pantalla
  Serial.print(valorLuminosidad); //Muestra el resultado de los calculos en pantalla
  Serial.println("%"); //muestra en pantalla
  delay(2500);
  //---------------------------------------------------------------------  
  //Acelerometro
  //---------------------------------------------------------------------
  // ---  Lectura acelerometro y giroscopio --- 
   uint8_t Buf[14];
   int FS_ACC = 16;
   int FS_GYRO = 2000;



   I2Cread(MPU9250_ADDRESS, 0x3B, 14, Buf);
 
//   // Convertir registros acelerometro
   float ax = (Buf[0] << 8 | Buf[1]);
   float ay = (Buf[2] << 8 | Buf[3]);
   float az = Buf[4] << 8 | Buf[5];

   ax = ax*FS_ACC/32768 ;
   ay = ay*FS_ACC/32768 ;
   az = az*FS_ACC/32768 ;
   //double mediax=(31.98+31.98+31.98+31.98+31.99)/5;
   //double axcalibrado=ax-mediax;
   double mediaz=(1.26+1.24+1.24+1.25+1.25+1.25)/6;  //calibración del offset (media)
   double azcalibrado=az-mediaz;
 
//   // Convertir registros giroscopio
   float gx = (Buf[8] << 8 | Buf[9]);
   float gy = (Buf[10] << 8 | Buf[11]);
   float gz = Buf[12] << 8 | Buf[13];
 
   gx = gx*FS_GYRO/32768;
   gy = gy*FS_GYRO/32768;
   gz = gz*FS_GYRO/32768;
   double mediagy=(1.04+1.28+1.16+1.34+0.92)/5;   //calibración del offset
   double gycalibrado=gy-mediagy;
   double mediagz=(3999.39+3999.45+3999.45+3999.27)/4; //calibración del offset
   double gzcalibrado=gz-mediagz;
// 
  Serial.println("//--------------------------------------------//");
  Serial.println("//-----------------Todo Gucci-----------------//");
  Serial.println("//--------------------------------------------//");
  ESP.deepSleep(sleepTimeS * 1000000);  //metodo que hace que se duerma por un tiempo 

}
