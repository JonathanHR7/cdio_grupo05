//Librerias
//----------------------------------
#include <Wire.h>    
#include <Adafruit_ADS1015.h>
//----------------------------------
//Esta funcion es la que se encarga de medir la humedad
int medirHumedad(Adafruit_ADS1015 ads1115,int pinSignal, int valorSeco, int valorHumedo){//Esta funcion recibe parametros definidos en el loop.
  
  int16_t adc0 = ads1115.readADC_SingleEnded(pinSignal);//Se define el puerto del ads1115 al que se va a conectar el sensor de humedad.
  int humedad = 100*valorSeco/(valorSeco-valorHumedo)-adc0*100/(valorSeco-valorHumedo);//Se calcula la humedad con su formula.

  //Este condicioal se hace para que no aparezcan valores por debajo de 0% y por encima de 100%
  if(humedad<0){
    humedad = humedad+10;
   }else if(humedad>100){
     humedad = 100;
   }
  
   return humedad; //devuelve humedad
  
}
//---------------------------------------------------------------------
//Salinidad
//---------------------------------------------------------------------
float medirSalinidad(Adafruit_ADS1015 ads1115, int pinSignal,int powerPin, int sinSal, int Salado){//Se pasa parametros definidos en el loop.
 
  float sumatorioLecturas=0.0, salinidad; //Se declaran las variables necesarias para realizar el calculo de la salinidad
  
    //Generar un pulso entre dos cables, de esta forma si la conductancia es mayor, querra decir que hay mucha salinidad de lo contrario poca.
    digitalWrite( powerPin, HIGH ); // Se enciende el pin 
    delay(100); // Se pone en espera 
    sumatorioLecturas = ads1115.readADC_SingleEnded(pinSignal);//Se toma lectura
    digitalWrite( powerPin, LOW ); // Se apaga el pin
    delay(10);

  //Serial.println(sumatorioLecturas);//Esta impresion por pantalla se ha utilizado para realizar la calibración del programa
  salinidad = 100*sinSal/(sinSal-Salado)-sumatorioLecturas*100/(sinSal-Salado);//Se saca el % con los calores máximos y minimos

  //Este condicioal se hace para que no aparezcan valores por debajo de 0% y por encima de 100%
  if(salinidad<0){
    salinidad=salinidad+10;
   }else if(salinidad>100){
      salinidad=100;  
    }
  
  return salinidad;//devuelve salinidad
}
double valorLeidoVoltaje(Adafruit_ADS1015 ads1115, int pinSignal, int voltajeMaximo, int bits){//pasamos los parametros los valores definidos en el loop.
  int16_t valorLeido = ads1115.readADC_SingleEnded(pinSignal);//se define el puerto
  double voltajeFinal;
  voltajeFinal=(valorLeido*voltajeMaximo)/bits;//fórmula que transforma el número recibido en voltaje.
  
  return voltajeFinal/1000; //devuelve el voltaje en SI 
}
double temperatura(double voltajeLeido,double b, double m){//Pasamos por parámetros los valores definidos en loop.
  double temperatura=(voltajeLeido-b)/m;  //formula que calcula la temperatura 
   //Calibración del programa
   if (temperatura<40){ //Para ajustar la temperatura y para calibrar el rango de error cuando es <40 se le resta 0.3
    temperatura=temperatura-0.3;
  }else{
    temperatura=temperatura+4.8; //si es >40 se le suma 4.8
    }
  return temperatura; //devuelve la temperatura 
  }
