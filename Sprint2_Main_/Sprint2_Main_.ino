#include "Sensores.h"  //incluye el fichero con las funciones 
Adafruit_ADS1115 ads1115(0x48);  //para usar la placa encima de la roja tenemos que instanciar y usar sus librerias .

void setup() {
  //configuracion de la ganancia 
  Serial.begin(9600);
  ads1115.begin();
  ads1115.setGain(GAIN_ONE);

}

void loop() {
  //Temperatura 
  //---------------------------------------------------------------------
  //constantes que utilizaremos para calcular el voltaje y la Temperatura
  int const VOLTAJEMAXIMO=4096;
  int const BITS= 32767;
  double const B=0.79;
  double const M= 0.033;
  int pinSignalVoltaje=2;  //el pin 2 que usamos en la placa.
  double voltajeLeido=valorLeidoVoltaje(ads1115, pinSignalVoltaje, VOLTAJEMAXIMO, BITS);//Hacemos llamada a la función dentro del .h
  double temperaturaLeida=temperatura(voltajeLeido,B,M); //Y le enviamos variables definidas anteriormente
  //---------------------------------------------------------------------
  //Humedad
  //---------------------------------------------------------------------
  //Se declaran como constantes los valores máximos y mínimos de humedad como constantes
  const int valorSeco = 20430;  // valor en aire
  const int valorHumedo = 9804;// valor en agua
  int pinSignalHumedad=0; //pin 0 de la placa
  int valorHumedad = medirHumedad(ads1115,pinSignalHumedad,valorSeco, valorHumedo);  //Hacemos llamada a la función
  //---------------------------------------------------------------------
  //Salinidad
  //---------------------------------------------------------------------
  const int sinSal = 7000;  // Valor sin sal
  const int Salado = 15000; //Valor con sal
  int pinSignalSalindad=1; //pin 1
  int powerPinSalinidad=5; //pin 5 que lee la conductancia entre los dos cables.
  float valorSalinidad = medirSalinidad(ads1115,pinSignalSalindad,powerPinSalinidad, sinSal, Salado); // Llamada y envio de variables a la función
  //---------------------------------------------------------------------
  //Humedad
  //---------------------------------------------------------------------
  Serial.print("La Humedad leidad es de (%): "); //Lo muestra en pantalla
  Serial.print(valorHumedad); //muestra en pantalla el valor de la Humedad
  Serial.println("%");       //muestra ("") en pantalla
  //---------------------------------------------------------------------  
  //Salinidad
  //---------------------------------------------------------------------
  Serial.print("La salinidad leida es de (%): "); //muestra en pantalla
  Serial.print(valorSalinidad); //Muestra el resultado de los calculos en pantalla
  Serial.println("%"); //muestra en pantalla
  //---------------------------------------------------------------------
  //Temperatura
  //---------------------------------------------------------------------
  Serial.print("La Temperatura es "); //muestra en pantalla
  Serial.print(temperaturaLeida); //muestra en pantalla la temperatura
  Serial.println("º");
  //---------------------------------------------------------------------
  delay(5000); //delay que tarda en actualizar la información
 
}
