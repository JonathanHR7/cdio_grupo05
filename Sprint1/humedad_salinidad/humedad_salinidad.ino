#include <Wire.h>
#include <Adafruit_ADS1015.h>
Adafruit_ADS1115 ads1115(0x48); // construct an ads1115 at address 0x48
int power_pin = 5;

void setup() {
  Serial.begin(9600);
  ads1115.begin();
  ads1115.setGain(GAIN_ONE);
  pinMode(power_pin, OUTPUT);

}

//Esta funcion es la que se encarga de medir la humedad
int medirHumedad(int pinSignal, int valorSeco, int valorHumedo){//Esta funcion recibe un parametro que es el puerto por donde se conecta el sensor y se espera la lectura, como en este caso por defecto es 0 se declara por defecto 0, en caso de que cambiase se pasaria por parametros el nuevo puerto
  int16_t adc0 = ads1115.readADC_SingleEnded(pinSignal);//Se define el puerto del ads1115 al que se va a conectar el sensor de humedad.
  int humedad = 100*valorSeco/(valorSeco-valorHumedo)-adc0*100/(valorSeco-valorHumedo);//Se hacen los calculos con el valor ledio y los valores máximos y mínimos

  //Este condicioal se hace para que no aparezcan valores por debajo de 0% y por encima de 100%
  if(humedad<0){
    humedad = humedad+10;
   }else if(humedad>100){
     humedad = 100;
   }
  
   return humedad;
  
}

float medirSalinidad(int pinSignal ,int powerPin, int minSal, int maxSal){//Se pasa por parametros los pines más el numero de lecturas que se van a tomar para hacer una media
  
  
  float sumatorioLecturas=0.0, salinidad; //Se declaran las variables necesarias para realizar el calculo de la salinidad
  
    //Lo que se pretende  es generar un pulso entre dos cables, de esta forma si la conductancia es mayor, querra decir que hay mucha salinidad de lo contrario poca
    digitalWrite( power_pin, HIGH ); // Se enciende el pin 
    delay(100); // Se pone en espera 
    sumatorioLecturas = ads1115.readADC_SingleEnded(pinSignal);//Se toma lectura
    digitalWrite( power_pin, LOW ); // Se apaga el pin
    delay(10);

  //Serial.println(sumatorioLecturas);//Esta impresion por pantalla se ha utilizado para realizar la calibración del programa
  salinidad = 100*minSal/(minSal-maxSal)-sumatorioLecturas*100/(minSal-maxSal);//Se saca el % con los calores máximos y minimos

  //Este condicioal se hace para que no aparezcan valores por debajo de 0% y por encima de 100%
  if(salinidad<0){
    salinidad=0;
   }else if(salinidad>100){
      salinidad=100;  
    }
  
  return salinidad;
}
  
void loop() {

  const int pinSignalHumedad = 0;
  const int minHumedo = 20430;
  const int maxHumedo = 9804;
  
  const int pinSignalSalinidad = 1;
  const int minSal = 1000;
  const int maxSal = 15000;
  
  int valorHumedad = medirHumedad(pinSignalHumedad,minHumedo,maxHumedo);
  float valorSalinidad = medirSalinidad(pinSignalSalinidad,power_pin,minSal,maxSal);
  
  Serial.print("La Humedad leidad es de (%): ");
  Serial.print(valorHumedad);
  Serial.println("%");
    

  Serial.print("La salinidad leida es de (%): ");
  Serial.print(valorSalinidad);
  Serial.println("%");

  delay(1000);

}
